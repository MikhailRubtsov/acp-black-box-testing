require 'rubygems'
require 'bundler'
require 'rspec'
require 'capybara'
require 'capybara/rspec'
require 'capybara/poltergeist'
require 'site_prism'
require 'yaml'

$: << File.expand_path('../..', __FILE__)
$: << File.expand_path('../support', __FILE__)

Dir['spec/sections/**/*.rb'].each { |f| require f }
Dir['spec/shared_contexts/**/*.rb'].each { |f| require f }
Dir['spec/shared_examples/**/*.rb'].each { |f| require f }
Dir['spec/pages/**/*.rb'].each { |f| require f }
Dir['spec/support/**/*.rb'].each { |f| require f }

Bundler.require(:default)

#Manage RSpec configuration
#//------------------------------------------------------------------------------------//
raise "RSPEC_ENV is not specified" unless ENV['RSPEC_ENV']

SCREENSHOT_DIR = Dir.getwd + '/rspec_screens'
FIXTURE_DIR = Dir.getwd + '/spec/support/fixtures'
CONFIG_FILE_PATH = File.expand_path('../support/configs/test_config.yml', __FILE__)
USER_DATA = YAML.load(File.open(CONFIG_FILE_PATH))[ENV['RSPEC_ENV']]
DRIVER_OPTIONS = {
    window_size: [1920, 1080],
    timeout: 30,
    js_errors: true,
    phantomjs_logger: '/dev/null'
}

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.syntax = :expect
  end

  config.mock_with :rspec do |mocks|
    mocks.allow_message_expectations_on_nil = true
    mocks.verify_partial_doubles = true
    mocks.syntax = :expect
  end

  config.alias_it_should_behave_like_to :it_has, 'has'
  config.filter_run :focus
  config.run_all_when_everything_filtered = true
  config.disable_monkey_patching!
  config.include JsInteraction, type: :feature
  Kernel.srand config.seed

#Manage Capybara configuration
#//------------------------------------------------------------------------------------//
  config.before(:suite) do
    Capybara.app_host = USER_DATA['URL']
    Capybara.run_server = false
    Capybara.register_driver :poltergeist do |app|
      Capybara::Poltergeist::Driver.new(app, DRIVER_OPTIONS)
    end
    Capybara.javascript_driver = :poltergeist
    FileUtils.rm_rf(SCREENSHOT_DIR)
    FileUtils.mkdir_p(SCREENSHOT_DIR)
  end

  config.before(:each) { page.driver.add_header("Accept-Encoding", "identity") }
end
