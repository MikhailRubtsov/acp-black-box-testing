RSpec.shared_context 'sample_context' do

  before(:all) { set_test_variables }

  subject(:super_admin)     { OpenStruct.new(name: dafault_user['USER_NAME'], email: dafault_user['USER_EMAIL'], password: dafault_user['PASSWORD']) }
  subject(:default_company) { OpenStruct.new(name: 'BURST', display_name: 'Burst Media', website: 'http://burstmedia.com') }

  def dafault_user
    USER_DATA['super_admin']
  end

  def set_test_variables
    %w(company user advertiser campaign).each do |test_var|
      instance_variable_set("@test_#{test_var}", send("fake_#{test_var}"))
    end
  end

  def set_samples(sample, count)
    count.times do |i|
      instance_variable_set("@sample_#{i+1}", send("fake_#{sample.to_s}"))
    end
  end

  def fake_line_item
    line_item = OpenStruct.new
    line_item.name = "Line Item ##{Faker::Name.last_name}"
    return line_item
  end

  def fake_company
    company = OpenStruct.new
    company.display_name = Faker::Company.name
    company.name = "#{company.display_name} #{Faker::Company.suffix}"
    company.website = "https://#{Faker::Internet.domain_name}"
    company.contact_name =  Faker::Name.name
    company.contact_email = Faker::Internet.email
    company.contact_phone = "+1492080206#{rand(9)}"
    company.logo = fixture_file("/company_assets/#{rand(10)}.png")
    company.description = Faker::Company.catch_phrase
    return company
  end

  def fake_campaign
    campaign = OpenStruct.new
    campaign.name = "#{Faker::Name.last_name}'s Campaign"
    campaign.start_date = Faker::Date.between(2.days.ago, Date.today)
    campaign.end_date = Faker::Date.forward(30)
    return campaign
  end

  def fake_advertiser
    advertiser = OpenStruct.new
    advertiser.name = "#{Faker::Name.first_name} #{Faker::Name.last_name}"
    return advertiser
  end

  def fake_user
    user = OpenStruct.new
    user.name = "#{Faker::Name.first_name} #{Faker::Name.last_name}"
    user.email = Faker::Internet.email(user.name)
    user.password = "changeme"
    user.time_zone = ActiveSupport::TimeZone.us_zones.sample.name
    return user
  end
end
