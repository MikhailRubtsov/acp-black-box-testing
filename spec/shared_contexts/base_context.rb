RSpec.shared_context 'base_context' do
  include_context   'sample_context'

  before(:each, test_type: :smoke) { login! }
  after(:each, test_type: :smoke)  { |example| take_screenshot(example) if example.exception.present?; logout! }

  let!(:home_page)  { DashboardPage.new }
  let!(:login_page) { LoginPage.new }

  def login!
    login_as(super_admin)
    expect(home_page).to be_displayed
  end

  def login_as(user)
    visit '/'
    expect(login_page).to be_displayed
    login_page.login(user.email, user.password)
    NavbarSection.set_user_name(user.name)
    NavbarSection.set_company_name(default_company.display_name)
  end

  def logout!
    home_page.load
    expect(home_page).to be_displayed
    home_page.navbar.unauthorize!
  end

  def take_screenshot(example)
    meta            = example.metadata
    filename        = File.basename(meta[:file_path])
    line_number     = meta[:line_number]
    screenshot_name = "screenshot-#{filename}-#{line_number}.png"
    screenshot_path = File.join(SCREENSHOT_DIR, screenshot_name)

    page.save_screenshot(screenshot_path)
    puts meta[:full_description] + "\n  Screenshot: #{screenshot_path}"
  end

  def fixture_file(path)
    FIXTURE_DIR + path
  end

  def set_id_from_url(obj)
    obj.id = page.current_path.split('/')[2]
    expect(obj.id).not_to eq nil
  end

  def set_id_from_table(obj, list)
    obj.id = list.find_in_list(obj).id.text
    expect(obj.id).not_to eq nil
  end
end

