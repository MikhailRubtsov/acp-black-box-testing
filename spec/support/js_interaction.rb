module JsInteraction
  def wait_for_ajax
    Timeout.timeout(Capybara.default_max_wait_time * 2) do
      loop until finished_all_ajax_requests?
    end
  end

  def finished_all_ajax_requests?
    page.evaluate_script('jQuery.active').zero?
  end

  def set_event_listener(table, event)
    page.execute_script <<-JS
      var table = $('#{table}').DataTable();
      table.on("#{event}.dt", function () {
          $('tbody:first').addClass("#{event}ed");
      } );
    JS
  end

  def drop_files(file_path)
    page.execute_script <<-JS
    fakeFileInput = window.$('<input/>').attr(
      {id: 'fakeFileInput', type:'file'}
    ).appendTo('body');
    JS
    attach_file('fakeFileInput', file_path)
    page.execute_script('var fileList = [fakeFileInput.get(0).files[0]]')
    page.execute_script <<-JS
      e = $.Event('drop');
      e.originalEvent = {dataTransfer : { files : fakeFileInput.get(0).files } };
      $('.dropzone').trigger(e);
    JS
  end
end

SitePrism::Page.send(:include, JsInteraction)
SitePrism::Section.send(:include, JsInteraction)