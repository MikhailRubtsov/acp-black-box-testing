RSpec.shared_examples 'base_example', with_example_context: true  do

  let(:home_page)                 { DashboardPage.new }
  let(:advertiser_index_page)     { AdvertiserIndexPage.new }
  let(:advertiser_new_page)       { AdvertiserNewPage.new }
  let(:advertiser_campaigns_page) { CampaignIndexPage.new }
  let(:campaign_new_page)         { CampaignNewPage.new }
  let(:campaign_show_page)        { CampaignShowPage.new }
  let(:line_item_new_page)        { LineItemNewPage.new }

  subject(:advertiser) { @test_advertiser }
  subject(:campaign)   { @test_campaign }
  subject(:line_item)  { @test_line_item }

  it "should create base data" do
    home_page.load
    expect(home_page).to be_displayed
    expect(home_page).to be_displayed
    click_link 'Advertisers'
    expect(advertiser_index_page).to be_displayed
    click_link 'New Advertiser'
    expect(advertiser_new_page).to be_displayed
    advertiser_new_page.form.submit_data_from(advertiser)
    expect(advertiser_campaigns_page).to be_displayed
    expect(advertiser_campaigns_page.flash.message).to eq('Advertiser was successfully created.')
    set_id_from_url advertiser
    advertiser_campaigns_page.load(advertiser)
    expect(advertiser_campaigns_page).to be_displayed
    click_link 'New campaign'
    expect(campaign_new_page).to be_displayed
    campaign_new_page.form.submit_data_from(campaign)
    expect(campaign_show_page).to be_displayed
    expect(campaign_show_page.flash.message).to eq('Campaign was successfully created.')
    set_id_from_url campaign
    click_link 'New Line Item'
    expect(line_item_new_page).to be_displayed
  end
end
