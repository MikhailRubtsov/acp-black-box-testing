RSpec.shared_examples 'cleaning_test_data'  do

  let(:advertiser_index_page) { AdvertiserIndexPage.new }

  subject(:advertiser) { @test_advertiser }

  it "should delete base data" do
    advertiser_index_page.load
    expect(advertiser_index_page).to be_displayed
    advertiser_index_page.advertisers_list_table.find_in_list(advertiser).click_link 'Delete'
    advertiser_index_page.wait_until_modal_visible(2)
    expect(advertiser_index_page.modal.body).to have_text "Do you really want to delete the advertiser '#{advertiser.name}'?"
    advertiser_index_page.modal.confirm!
    index_page.wait_for_ajax
  end
end
