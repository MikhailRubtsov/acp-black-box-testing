require 'spec/spec_helper'

RSpec.feature 'line item CRUDD UI test', with_example_context: true, test_type: :smoke, js: true do
  include_context   'base_context'

  let(:index_page) { CampaignShowPage.new }
  let(:new_page)   { LineItemNewPage.new }
  let(:show_page)  { LineItemShowPage.new }
  let(:edit_page)  { LineItemEditPage.new }
  let(:unit_page)  { UnitPage.new }

  subject(:campaign) { @test_campaign }

  before(:all) { set_samples(:line_item, 2) }

  context 'when create line item' do
    it "Line item new page showuld be avalible from campaign page" do
      index_page.load(campaign)
      expect(index_page).to be_displayed
      click_link 'New Line Item'
      expect(new_page).to be_displayed
    end

    it "should displays error message if user not valid" do
      index_page.load(campaign)
      click_link 'New Line Item'
      expect(new_page).to be_displayed
      click_button 'Save'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should display new unit page after create" do
      new_page.load(campaign)
      expect(new_page).to be_displayed
      new_page.form.set_platform('Desktop/Mobile Web')
      new_page.form.set_unit_type('Expandable')
      new_page.form.enable_publisher!
      new_page.form.enable_moat!
      new_page.form.submit_data_from(@sample_1, campaign)
      expect(unit_page).to be_displayed
      expect(unit_page.flash.message).to eq('Line Item was successfully created.')
      set_id_from_url @sample_1
    end

    it "should save all attributes currect" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      expect(edit_page.form.name_input.value).to eq(@sample_1.name)
      expect(edit_page.form.start_date_input.value).to eq(@test_campaign.start_date.strftime('%Y-%m-%d'))
      expect(edit_page.form.end_date_input.value).to eq(@test_campaign.end_date.strftime('%Y-%m-%d'))
    end
  end

  context 'when update line item' do
    it "should displays error message if Line item's not valid" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.name_input.set ''
      click_button 'Save'
      expect(edit_page).to have_text("Name can't be blank")
    end

    it "should display new unit page after update" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.submit_data_from(@sample_2, campaign)
      expect(show_page).to be_displayed
      expect(show_page.flash.message).to eq('Line Item was successfully updated.')
      set_id_from_url @sample_2
    end

    it "should save all attributes currect" do
      edit_page.load(@sample_2)
      expect(edit_page).to be_displayed
      expect(edit_page.form.name_input.value).to eq(@sample_2.name)
      expect(edit_page.form.start_date_input.value).to eq(@test_campaign.start_date.strftime('%Y-%m-%d'))
      expect(edit_page.form.end_date_input.value).to eq(@test_campaign.end_date.strftime('%Y-%m-%d'))
    end
  end

  context 'when duplicate line item' do
    it "should able to duplicate line item" do
      show_page.load(@sample_2)
      expect(show_page).to be_displayed
      click_link 'Duplicate Line Item'
      show_page.wait_until_modal_visible(2)
      expect(show_page.modal.body).to have_text "Please confirm duplication of '#{@sample_2.name}'"
      show_page.modal.confirm!
      show_page.wait_for_ajax
      expect(edit_page).to be_displayed
      expect(edit_page.header).to have_text "Edit Line Item #{@sample_2.name} Copy 1"
      expect(edit_page.form.name_input.value).to eq("#{@sample_2.name} Copy 1")
      expect(edit_page.form.start_date_input.value).to eq(@test_campaign.start_date.strftime('%Y-%m-%d'))
      expect(edit_page.form.end_date_input.value).to eq(@test_campaign.end_date.strftime('%Y-%m-%d'))
    end

    it "should display duplicated line item on index_page" do
      index_page.load(campaign)
      expect(index_page).to be_displayed
      expect(index_page.find_in_list "#{@sample_2.name} Copy 1").not_to eq nil
    end
  end

  context 'when delete line item' do
    it "should delete line_item with warning message" do
      index_page.load(campaign)
      expect(index_page).to be_displayed
      index_page.find_in_list(@sample_2.id).click_link 'Delete'
      index_page.wait_until_modal_visible
      expect(index_page.modal.body).to have_text "Do you really want to delete the line item '#{@sample_2.name}'?"
      index_page.modal.confirm!
      index_page.wait_for_ajax
      expect(index_page).to be_displayed
    end

    it_behaves_like "cleaning_test_data"
  end
end