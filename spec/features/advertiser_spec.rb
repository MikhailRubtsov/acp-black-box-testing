require 'spec/spec_helper'

RSpec.feature 'advertiser CRUD UI test', test_type: :smoke, js: true do
  include_context   'base_context'

  let(:index_page) { AdvertiserIndexPage.new }
  let(:new_page)   { AdvertiserNewPage.new }
  let(:show_page)  { AdvertiserShowPage.new }
  let(:edit_page)  { AdvertiserEditPage.new }

  let(:advertiser_campaigns_page) { CampaignIndexPage.new }

  before(:all) { set_samples(:advertiser, 2) }

  context 'when create advertiser' do
    it "should be available from navigation bar" do
      home_page.load
      expect(home_page).to be_displayed
      click_link 'Advertisers'
      expect(index_page).to be_displayed
    end

    it "should displays error message if advertiser not valid" do
      index_page.load
      click_link 'New Advertiser'
      expect(new_page).to be_displayed
      click_button 'Save'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should display advertiser's campaigns page after create" do
      new_page.load
      expect(new_page).to be_displayed
      new_page.form.submit_data_from(@sample_1)
      expect(advertiser_campaigns_page).to be_displayed
      expect(advertiser_campaigns_page.flash.message).to eq('Advertiser was successfully created.')
      set_id_from_url @sample_1
    end

    it "should save all attributes currect" do
      show_page.load(@sample_1)
      expect(show_page).to be_displayed
      expect(show_page.header.text).to eql "Campaigns of #{@sample_1.name}"
    end
  end

  context 'when update advertiser' do
    it "should displays error message if advertiser not valid" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.name_input.set ''
      click_button 'Save'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should display advertiser show page after update" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.submit_data_from(@sample_2)
      expect(advertiser_campaigns_page).to be_displayed
      expect(advertiser_campaigns_page.flash.message).to eq('Advertiser was successfully updated.')
      set_id_from_url @sample_2
    end

    it "should save all attributes currect" do
      show_page.load(@sample_2)
      expect(show_page).to be_displayed
      expect(show_page.header.text).to eql "Campaigns of #{@sample_2.name}"
    end
  end

  context 'when delete advertiser' do
    it "should delete advertiser with warning message" do
      index_page.load
      expect(index_page).to be_displayed
      index_page.advertisers_list_table.find_in_list(@sample_2).click_link 'Delete'
      index_page.wait_until_modal_visible(2)
      expect(index_page.modal.body).to have_text "Do you really want to delete the advertiser '#{@sample_2.name}'?"
      index_page.modal.confirm!
      index_page.wait_for_ajax
      expect(index_page).to be_displayed
    end
  end
end