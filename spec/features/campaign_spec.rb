require 'spec/spec_helper'

RSpec.feature 'campaign CRUD UI test', with_example_context: true, test_type: :smoke, js: true do
  include_context   'base_context'

  let(:index_page) { CampaignIndexPage.new }
  let(:new_page)   { CampaignNewPage.new }
  let(:show_page)  { CampaignShowPage.new }
  let(:edit_page)  { CampaignEditPage.new }

  subject(:advertiser) { @test_advertiser }

  before(:all) { set_samples(:campaign, 2) }

  context 'when create campaign' do
    it "Campaign new page showuld be avalible from advertiser page" do
      index_page.load(advertiser)
      expect(index_page).to be_displayed
      click_link 'New campaign'
      expect(new_page).to be_displayed
    end

    it "should displays error message if Campaign not valid" do
      new_page.load(advertiser)
      expect(new_page).to be_displayed
      click_button 'Save'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should display Campaign's line items page after create" do
      new_page.load(advertiser)
      expect(new_page).to be_displayed
      new_page.form.submit_data_from(@sample_1)
      expect(show_page).to be_displayed
      expect(show_page.flash.message).to eq('Campaign was successfully created.')
      set_id_from_url @sample_1
    end

    it "should save all attributes currect" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      expect(edit_page.form.name_input.value).to eq(@sample_1.name)
      expect(edit_page.form.start_date_input.value).to eq(@sample_1.start_date.strftime('%Y-%m-%d'))
      expect(edit_page.form.end_date_input.value).to eq(@sample_1.end_date.strftime('%Y-%m-%d'))
    end
  end

  context 'when update campaign' do
    it "should displays error message if Campaign not valid" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.name_input.set ''
      click_button 'Save'
      expect(edit_page).to have_text("Name can't be blank")
    end

    it "should display Campaign's line items page after update" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.submit_data_from(@sample_2)
      expect(show_page).to be_displayed
      expect(show_page.flash.message).to eq('Campaign was successfully updated.')
      set_id_from_url @sample_2
    end

    it "should save all attributes currect" do
      edit_page.load(@sample_2)
      expect(edit_page).to be_displayed
      expect(edit_page.form.name_input.value).to eq(@sample_2.name)
      expect(edit_page.form.start_date_input.value).to eq(@sample_2.start_date.strftime('%Y-%m-%d'))
      expect(edit_page.form.end_date_input.value).to eq(@sample_2.end_date.strftime('%Y-%m-%d'))
    end
  end

  context 'when delete campaign' do
    it "should delete Campaign with warning message" do
      index_page.load(advertiser)
      expect(index_page).to be_displayed
      index_page.campaign_list_table.find_in_list(@sample_2).click_link 'Delete'
      index_page.wait_until_modal_visible(2)
      expect(index_page.modal.body).to have_text "Do you really want to delete the campaign '#{@sample_2.name}'?"
      index_page.modal.confirm!
      index_page.wait_for_ajax
      expect(index_page).to be_displayed
    end
    it_behaves_like "cleaning_test_data"
  end
end