require 'spec/spec_helper'

RSpec.feature 'user CRUD UI test', test_type: :smoke, js: true do
  include_context   'base_context'

  let(:index_page) { UserIndexPage.new }
  let(:new_page)   { UserNewPage.new }
  let(:show_page)  { UserShowPage.new }
  let(:edit_page)  { UserEditPage.new }
  let(:home_page)  { DashboardPage.new }

  before(:all) { set_samples(:user, 2) }

  context 'when create user' do
    it "should be available from navigation bar" do
      home_page.load
      expect(home_page).to be_displayed
      click_link 'Admin'
      click_link 'Manage Users'
      expect(index_page).to be_displayed
    end

    it "should displays error message if user not valid" do
      index_page.load
      click_link 'New User'
      expect(new_page).to be_displayed
      click_button 'Save'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should display user index page after create" do
      new_page.load
      expect(new_page).to be_displayed
      new_page.form.set_role('Creative Services')
      new_page.form.submit_data_from(@sample_1, default_company)
      expect(index_page).to be_displayed
      expect(index_page.flash.message).to eq('User was successfully created.')
      set_id_from_table(@sample_1, index_page.users_list_table)
    end

    it "should autorise with created user" do
      logout!
      login_as(@sample_1)
      expect(home_page).to be_displayed
      expect(home_page.flash.message).to eq('Signed in successfully.')
    end
  end

  context 'when update user' do
    it "should displays error message if user not valid" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.name_input.set ''
      click_button 'Save'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should change user" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.submit_data_from(@sample_2, default_company)
      expect(index_page).to be_displayed
      expect(index_page.flash.message).to eq('User was successfully updated.')
      set_id_from_table(@sample_2, index_page.users_list_table)
    end

    it "should autorise with created user" do
      logout!
      login_as(@sample_2)
      expect(home_page).to be_displayed
      expect(home_page.flash.message).to eq('Signed in successfully.')
    end

    it "should save all attributes currect" do
      edit_page.load(@sample_2)
      expect(edit_page).to be_displayed
      expect(edit_page.form.name_input.value).to eq(@sample_2.name)
      expect(edit_page.form.email_input.value).to eq(@sample_2.email)
    end
  end

  context 'when delete user' do
    it "should delete user with warning message" do
      index_page.load
      expect(index_page).to be_displayed
      index_page.users_list_table.find_in_list(@sample_2).click_link 'Delete'
      index_page.wait_until_modal_visible(2)
      expect(index_page.modal.body).to have_text "Do you really want to delete user '#{@sample_2.name}'?"
      index_page.modal.confirm!
      index_page.wait_for_ajax
      expect(index_page).to be_displayed
    end
  end
end