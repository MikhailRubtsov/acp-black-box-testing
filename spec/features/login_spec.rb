require 'spec/spec_helper'

RSpec.feature 'authorization test', js: true do
  include_context 'base_context'

  let(:login_page) { LoginPage.new }
  let(:home_page)  { DashboardPage.new }

  context 'when authorize' do
    it "User can login and logout" do
      login!
      expect(home_page).to be_displayed
      expect(home_page.flash.message).to eq('Signed in successfully.')
      logout!
      expect(login_page).to be_displayed
      expect(login_page.flash.message).to eq('You need to sign in or sign up before continuing.')
    end

    it "User can't login if it not registered" do
      login_as(fake_user)
      expect(login_page).to be_displayed
      expect(login_page.flash.message).to eq('Invalid email or password.')
    end
  end
end