require 'spec/spec_helper'

RSpec.feature 'company CRUD UI test', test_type: :smoke, js: true do
  include_context   'base_context'

  let(:index_page) { CompanyIndexPage.new }
  let(:new_page)   { CompanyNewPage.new }
  let(:show_page)  { CompanyShowPage.new }
  let(:edit_page)  { CompanyEditPage.new }

  before(:all) { set_samples(:company, 2) }

  context 'when create company' do
    it "index page should be available from navigation bar" do
      home_page.load
      expect(home_page).to be_displayed
      click_link 'Admin'
      click_link 'Manage Companies'
      expect(index_page).to be_displayed
    end

    it "should contain base company on load" do
      index_page.load
      expect(index_page).to be_displayed
      index_page.companies_list_table.find_in_list(default_company).click_on default_company.name
      expect(show_page).to be_displayed
    end

    it "should displays error message if company not valid" do
      new_page.load
      expect(new_page).to be_displayed
      click_button 'Create Company'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should display index page after create" do
      new_page.load
      expect(new_page).to be_displayed
      new_page.logo_uploader.drop_files @sample_1.logo
      new_page.form.submit_data_from(@sample_1)
      expect(show_page).to be_displayed
      expect(show_page.flash.message).to eq('Company was successfully created.')
      set_id_from_url @sample_1
    end

    it "should save all attributes currect" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      expect(edit_page.form.name_input.value).to eq(@sample_1.name)
      expect(edit_page.form.display_name_input.value).to eq(@sample_1.display_name)
      expect(edit_page.form.website_input.value).to eq(@sample_1.website)
      expect(edit_page.form.contact_name_input.value).to eq(@sample_1.contact_name)
      expect(edit_page.form.contact_email_input.value).to eq(@sample_1.contact_email)
      expect(edit_page.form.description_input.value).to eq(@sample_1.description)
    end

    it "should be able to swith to created company" do
      index_page.load
      expect(index_page).to be_displayed
      index_page.navbar.switch_company @sample_1
      expect(page).to have_text @sample_1.upcase
    end
  end

  context 'when update company' do
    it "should displays error message if company not valid" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.name_input.set ''
      click_button 'Update Company'
      expect(index_page).to be_displayed
      expect(index_page).to have_text("Name can't be blank")
    end

    it "should change company" do
      edit_page.load(@sample_1)
      expect(edit_page).to be_displayed
      edit_page.form.submit_data_from(@sample_2)
      expect(index_page).to be_displayed
      expect(index_page.flash.message).to eq('Company was successfully updated.')
      set_id_from_url @sample_2
    end

    it "should be able to swith to created company" do
      index_page.load
      expect(index_page).to be_displayed
      index_page.navbar.switch_company @sample_2
      expect(page).to have_text @sample_2.upcase
    end

    it "should save all attributes currect" do
      edit_page.load(@sample_2)
      expect(edit_page).to be_displayed
      expect(edit_page.form.name_input.value).to eq(@sample_2.name)
      expect(edit_page.form.display_name_input.value).to eq(@sample_2.display_name)
      expect(edit_page.form.website_input.value).to eq(@sample_2.website)
      expect(edit_page.form.contact_name_input.value).to eq(@sample_2.contact_name)
      expect(edit_page.form.contact_email_input.value).to eq(@sample_2.contact_email)
      expect(edit_page.form.description_input.value).to eq(@sample_2.description)
    end
  end

  context 'when delete company' do
    it "should delete company with warning message" do
      index_page.load
      expect(index_page).to be_displayed
      index_page.companies_list_table.find_in_list(@sample_2).click_link 'Delete'
      index_page.wait_until_modal_visible(2)
      expect(index_page.modal.body).to have_text "Do you really want to delete the company '#{@sample_2.name}' ?"
      index_page.modal.confirm!
      index_page.wait_for_ajax
      expect(index_page).to be_displayed
    end
  end
end