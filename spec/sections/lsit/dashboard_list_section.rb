class DashboardListSection < SitePrism::Section
  class DashboardCollection < SitePrism::Section
    element :id, :xpath, './/td[count(//table/thead/tr/th[.="ID"]/preceding-sibling::th)+1]'
    element :product_type, :xpath, './/td[count(//table/thead/tr/th[.="Product type"]/preceding-sibling::th)+1]'
    element :in_revision, :xpath, './/td[count(//table/thead/tr/th[.="Name"]/preceding-sibling::th)+1]/i'
    element :name, :xpath, './/td[count(//table/thead/tr/th[.="Name"]/preceding-sibling::th)+1]/a'
    element :start, :xpath, './/td[count(//table/thead/tr/th[.="Start"]/preceding-sibling::th)+1]'
    element :end, :xpath, './/td[count(//table/thead/tr/th[.="End"]/preceding-sibling::th)+1]'
    element :status, :xpath, './/td[count(//table/thead/tr/th[.="Status"]/preceding-sibling::th)+1]'
    element :advertiser, :xpath, './/td[count(//table/thead/tr/th[.="Advertiser"]/preceding-sibling::th)+1]/a'
    element :campaign, :xpath, './/td[count(//table/thead/tr/th[.="Campaign"]/preceding-sibling::th)+1]/a'
    element :edit_button, :xpath, './/td[count(//table/thead/tr/th[.=""]/preceding-sibling::th)+1]/a[text()="Edit"]'
    element :edit_unit_button, :xpath, './/td[count(//table/thead/tr/th[.=""]/preceding-sibling::th)+1]/a[text() = "Edit Unit" or text() = "Add Unit"]'
    element :delete_unit_button, :xpath, './/td[count(//table/thead/tr/th[.=""]/preceding-sibling::th)+1]/a[text() = "Delete"]'
    element :more_button, :xpath, './/td[count(//table/thead/tr/th[.=""]/preceding-sibling::th)+1]/div/a[text() = "More..."]'
    element :preview_dropdown_item, :xpath, './/td[count(//table/thead/tr/th[.=""]/preceding-sibling::th)+1]/div//a[text() = "Preview"]'
    element :public_preview_dropdown_item, :xpath, './/td[count(//table/thead/tr/th[.=""]/preceding-sibling::th)+1]/div//a[text() = "Public Preview"]'
    element :send_for_approval_dropdown_item, :xpath, './/td[count(//table/thead/tr/th[.=""]/preceding-sibling::th)+1]/div//a[text() = "Send for Approval"]'
  end

  element :header, :xpath, './/thead/tr'
  element :body, :xpath, './/tbody'
  element :search_field, :xpath, '//INPUT[@name="search"]'

  sections :items, DashboardCollection, :xpath, './/tbody/tr'
end