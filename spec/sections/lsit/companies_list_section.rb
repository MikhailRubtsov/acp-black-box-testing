class CompaniesListSection < SitePrism::Section
   class CompaniesCollection < SitePrism::Section
     element :id, :xpath, './/td[1]'
     element :name, :xpath, './/td[2]/a'
   end

   element :header, :xpath, './/thead/tr'
   element :body, :xpath, './/tbody'
   element :search_field, :xpath, '//INPUT[@name="search"]'

   sections :items, CompaniesCollection, :xpath, './/tbody/tr'

   element :filter_event, 'tbody#searched'

   def find_in_list(company)
     set_event_listener('#companies', 'search')
     search_field.set company.name
     wait_for_filter_event
     items.find {|item| item.name.text.eql? company.name}
   end
end