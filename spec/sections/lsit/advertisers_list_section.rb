class AdvertisersListSection < SitePrism::Section
  class AdvertisersCollection < SitePrism::Section
    element :id, :xpath, './/td[1]'
    element :name, :xpath, './/td[2]/a'
    element :campaigns_count, :xpath, './/td[3]'
    element :line_items_count, :xpath, './/td[4]'
  end

  element :header, :xpath, './/thead/tr'
  element :body, :xpath, './/tbody'
  element :search_field, :xpath, '//INPUT[@name="search"]'

  sections :items, AdvertisersCollection, :xpath, './/tbody/tr'

  element :filter_event, 'tbody#searched'

  def find_in_list(advertiser)
    set_event_listener('#advertiser', 'search')
    search_field.set advertiser.name
    wait_for_filter_event
    items.find {|item| item.name.text.eql? advertiser.name}
  end
end