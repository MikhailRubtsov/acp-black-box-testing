class UsersListSection < SitePrism::Section
  class UsersCollection < SitePrism::Section
    element :id, :xpath, './/td[1]'
    element :name, :xpath, './/td[2]/a'
  end

  element :header, :xpath, './/thead/tr'
  element :body, :xpath, './/tbody'
  element :search_field, :xpath, '//INPUT[@name="search"]'

  sections :items, UsersCollection, :xpath, './/tbody/tr'

  element :filter_event, 'tbody#searched'

  def find_in_list(user)
    set_event_listener('#users', 'search')
    search_field.set user.name
    wait_for_filter_event
    items.find {|item| item.name.text.eql? user.name}
  end
end