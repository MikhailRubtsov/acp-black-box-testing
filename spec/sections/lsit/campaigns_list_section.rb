class CampaignsListSection < SitePrism::Section
  class CampaignsCollection < SitePrism::Section
    element :id, :xpath, './/td[1]'
    element :name, :xpath, './/td[2]/a'
  end

  element :header, :xpath, './/thead/tr'
  element :body, :xpath, './/tbody'
  element :search_field, :xpath, '//INPUT[@name="search"]'

  sections :items, CampaignsCollection, :xpath, './/tbody/tr'

  element :filter_event, 'tbody#searched'

  def find_in_list(campaign)
    set_event_listener('#campaigns', 'search')
    search_field.set campaign.name
    wait_for_filter_event
    items.find {|item| item.name.text.eql? campaign.name}
  end
end