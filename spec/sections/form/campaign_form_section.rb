class CampaignFormSection < SitePrism::Section

  element :name_input, '#campaign_name'
  element :start_date_input, '#campaign_start_date'
  element :end_date_input, '#campaign_end_date'

  def submit_data_from(campaign)
    name_input.set campaign.name
    page.execute_script("$('#campaign_start_date').val('#{campaign.start_date}')")
    page.execute_script("$('#campaign_end_date').val('#{campaign.end_date}')")
    click_button 'Save'
  end
end