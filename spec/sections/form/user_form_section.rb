class UserFormSection < SitePrism::Section

  element :name_input, '#user_name'
  element :email_input, '#user_email'
  element :password_input, '#user_password'
  element :password_confirmation_input, '#user_password_confirmation'
  element :time_zone_selector, '#user_time_zone'
  element :company_selector, '#target_company'
  element :super_admin_swith, :xpath, '//*fieldset[3]/fieldset[1]/div[1]/div'

  elements :user_roles, 'fieldset.roles div.checkbox'

  def submit_data_from(user, company)
    name_input.set user.name
    email_input.set user.email
    password_input.set user.password
    time_zone_selector.select user.time_zone
    password_confirmation_input.set user.password
    company_selector.select company.display_name
    click_button 'Save'
  end

  def set_role(role)
    user_roles.find { |el| el.text.match /#{role}/}.first('div.bootstrap-switch-container').click
  end
end