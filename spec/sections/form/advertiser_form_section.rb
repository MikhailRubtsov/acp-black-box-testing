class AdvertiserFormSection < SitePrism::Section

  element :name_input, '#advertiser_name'

  def submit_data_from(advertiser)
    name_input.set advertiser.name
    click_button 'Save'
  end
end