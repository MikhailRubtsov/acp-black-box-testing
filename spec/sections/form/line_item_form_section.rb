class LineItemFormSection < SitePrism::Section
  element :name_input, '#line_item_name'
  element :campaign_selector, '#line_item_campaign_id'
  element :start_date_input, '#line_item_start_date'
  element :end_date_input, '#line_item_end_date'
  element :platform_selector, '#line_item_platform'
  element :unit_type_selector, '#line_item_unit_type'
  element :enable_publisher_switch, 'div.enable-publisher'
  element :enable_moat_switch, 'div.enable-moat'

  def submit_data_from(line_item, campaign)
    name_input.set line_item.name
    campaign_selector.select campaign.name
    page.execute_script("$('#line_item_start_date').val('#{campaign.start_date}')")
    page.execute_script("$('#line_item_end_date').val('#{campaign.end_date}')")
    click_button 'Save'
  end

  def set_platform(val)
    platform_selector.select val
  end

  def set_unit_type(val)
    unit_type_selector.select val
  end

  def enable_publisher!
    enable_publisher_switch.first('div.bootstrap-switch-container').click
  end

  def enable_moat!
    enable_moat_switch.first('div.bootstrap-switch-container').click
  end
end