class CompanyFormSection < SitePrism::Section
  
  element :name_input, '#company_name'
  element :display_name_input, '#company_display_name'
  element :website_input, '#company_website'
  element :contact_name_input, '#company_contact_name'
  element :contact_email_input, '#company_contact_email'
  element :contact_phone_input, '#company_contact_phone'
  element :description_input, '#company_description'

  element :submit_button, :xpath, '//INPUT[@type="submit"]'


  def submit_data_from(company)
    name_input.set company.name
    display_name_input.set company.display_name
    website_input.set company.website
    contact_name_input.set company.contact_name
    contact_email_input.set company.contact_email
    contact_phone_input.set company.contact_phone
    description_input.set company.description
    submit_button.click
  end

  def visit_tab(tab_label)
    click_link tab_label
  end
end