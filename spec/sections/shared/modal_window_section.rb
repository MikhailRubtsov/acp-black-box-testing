class ModalWindowSection < SitePrism::Section
  element :label, 'h3#confirmation-label'
  element :body, 'div#confirmation-body'
  element :confirm_button, 'button#confirmation-confirm'

  def confirm!
    click_button 'Yes'
  end
end