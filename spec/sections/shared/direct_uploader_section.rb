class DirectUploaderSection < SitePrism::Section

  element :drop_zone, '.dropzone'
  element :progressbar, '.progress'
  element :preview, '.preview-wrapper'
  element :delete_button, 'a.delete-image'

  def upload_file(file)
    drop_files(file)
    wait_for_progressbar
    wait_until_progressbar_invisible(120)
  end
end