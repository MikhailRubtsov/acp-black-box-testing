class NavbarSection < SitePrism::Section

  element :brand_icon, 'a.navbar-brand'

  def self.set_user_name(value)    
    element :user_dropdown, :xpath, %Q%//a[text()='#{value.split.map(&:capitalize)*' '}']%
  end

  def self.set_company_name(value)    
    element :company_dropdown, :xpath, %Q%//span[text()='#{value}']%
  end

  def unauthorize!
    user_dropdown.click
    click_link 'Logout'
  end

  def switch_company(company)
    company_dropdown.click
    first(:link, company.display_name).click
    NavbarSection.set_company_name(company.display_name)
  end
end