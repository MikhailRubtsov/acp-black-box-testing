class FlashMessageSection < SitePrism::Section
  elements :messages, "div.alert > div"

  def all_messages
    messages.map(&:text)
  end

  def message
    all_messages.first
  end
end