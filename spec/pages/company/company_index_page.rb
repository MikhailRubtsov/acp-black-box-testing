class CompanyIndexPage < SitePrism::Page
  set_url '/companies'
  set_url_matcher %r{/companies}

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
  section :companies_list_table, CompaniesListSection, :xpath, '//table[@id="companies"]'
  section :navbar, NavbarSection, 'nav.navbar'
  section :modal, ModalWindowSection, 'div#confirmation-modal'
end