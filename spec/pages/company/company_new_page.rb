class CompanyNewPage < SitePrism::Page
  set_url '/companies/new'
  set_url_matcher %r{/companies/new}

  section :logo_uploader, DirectUploaderSection, '.company_logo'
  section :form, CompanyFormSection, 'form.new_company'
end