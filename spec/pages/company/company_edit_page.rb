class CompanyEditPage < SitePrism::Page
  set_url 'companies/{company_id}/edit'
  set_url_matcher /companies\/\w{2,5}-[a-zA-Z0-9]{10}\/edit/

  section :logo_uploader, DirectUploaderSection, '.company_logo'
  section :form, CompanyFormSection, 'form.edit_company'

  def load(company)
    super(company_id: company.id)
  end
end