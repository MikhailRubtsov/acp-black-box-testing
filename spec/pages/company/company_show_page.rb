class CompanyShowPage < SitePrism::Page
  set_url '/companies/{company_id}'
  set_url_matcher  /companies\/\w{2,5}-[a-zA-Z0-9]{10}/

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
end