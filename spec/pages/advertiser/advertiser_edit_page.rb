class AdvertiserEditPage < SitePrism::Page
  set_url '/advertisers/{advertiser_id}/edit'
  set_url_matcher /advertisers\/\w{2,5}-[a-zA-Z0-9]{10}\/edit/

  section :form, AdvertiserFormSection, 'form.edit_advertiser'

  def load(advertiser)
    super(advertiser_id: advertiser.id)
  end
end