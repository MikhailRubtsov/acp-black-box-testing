class AdvertiserIndexPage < SitePrism::Page
  set_url '/advertisers'
  set_url_matcher %r{/advertisers}

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
  section :modal, ModalWindowSection, 'div#confirmation-modal'
  section :advertisers_list_table, AdvertisersListSection, 'table#advertisers.datatable'
end