class AdvertiserShowPage < SitePrism::Page
  set_url '/advertisers/{advertiser_id}'
  set_url_matcher  /advertisers\/\w{2,5}-[a-zA-Z0-9]{10}/

  element :header, 'h2'

  def load(advertiser)
    super(advertiser_id: advertiser.id)
  end
end