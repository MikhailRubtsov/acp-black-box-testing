class AdvertiserNewPage < SitePrism::Page
  set_url '/advertisers/new'
  set_url_matcher %r{/advertisers/new}

  section :form, AdvertiserFormSection, 'form.new_advertiser'
end