class UnitPage < SitePrism::Page
  set_url '/line_items/{line_item_id}/{unit_name}/new'
  set_url_matcher /\/line_items\/\w{2,5}-[a-zA-Z0-9]{10}\/expandables\/new/

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
end