class LoginPage < SitePrism::Page
  set_url "/d/users/sign_in"
  set_url_matcher %r{/d/users/sign_in}

  element :login_form, 'form#new_user'

  element :email_input, '#user_email'
  element :password_input, '#user_password'
  element :remember_input, '#user_remember_me'

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'

  def login(email, password)
    email_input.set email
    password_input.set password
    click_button 'Sign in'
  end
end
