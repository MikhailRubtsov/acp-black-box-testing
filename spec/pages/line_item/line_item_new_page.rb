class LineItemNewPage < SitePrism::Page
  set_url '/campaigns/{campaign_id}/line_items/new'
  set_url_matcher /campaigns\/\w{2,5}-[a-zA-Z0-9]{10}\/line_items\/new/

  section :form, LineItemFormSection, 'form.new_line_item'

  def load(campaign)
    super(campaign_id: campaign.id)
  end
end