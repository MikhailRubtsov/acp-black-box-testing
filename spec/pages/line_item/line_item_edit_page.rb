class LineItemEditPage < SitePrism::Page
  set_url '/line_items/{line_item_id}/edit'
  set_url_matcher /\/line_items\/\w{2,5}-[a-zA-Z0-9]{10}\/edit/

  section :form, LineItemFormSection, 'form.edit_line_item'
  element :header, '.page-heading'

  def load(line_item)
    super(line_item_id: line_item.id)
  end
end