class LineItemShowPage < SitePrism::Page
  set_url '/line_items/{line_item_id}'
  set_url_matcher /\/line_items\/\w{2,5}-[a-zA-Z0-9]{10}/

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
  section :modal, ModalWindowSection, 'div#confirmation-modal'
  element :header, '.page-heading'

  def load(line_item)
    super(line_item_id: line_item.id)
  end
end