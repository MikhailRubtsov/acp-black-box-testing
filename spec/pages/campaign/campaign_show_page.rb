class CampaignShowPage < SitePrism::Page
  set_url '/campaigns/{campaign_id}/line_items'
  set_url_matcher /campaigns\/\w{2,5}-[a-zA-Z0-9]{10}\/line_items/

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
  section :modal, ModalWindowSection, 'div#confirmation-modal'
  elements :line_items, '#line-items tr'
  element :draw_event, 'tbody#drawed'

  def load(campaign)
    super(campaign_id: campaign.id)
    set_event_listener('#line_items', 'draw')
    wait_for_draw_event
  end

  def find_in_list(text)
    line_items.find { |el| el.text.match /#{text}/}
  end
end