class CampaignIndexPage < SitePrism::Page
  set_url '/advertisers/{advertiser_id}/campaigns'
  set_url_matcher /advertisers\/\w{2,5}-[a-zA-Z0-9]{10}\/campaigns/

  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
  section :campaign_list_table, CampaignsListSection, :xpath, '//table[@id="campaigns"]'
  section :modal, ModalWindowSection, 'div#confirmation-modal'

  def load(advertiser)
    super(advertiser_id: advertiser.id)
  end
end