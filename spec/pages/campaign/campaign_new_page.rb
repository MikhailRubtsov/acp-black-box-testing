class CampaignNewPage < SitePrism::Page
  set_url '/advertisers/{advertiser_id}/campaigns/new'
  set_url_matcher /advertisers\/\w{2,5}-[a-zA-Z0-9]{10}\/campaigns\/new/

  section :form, CampaignFormSection, 'form.new_campaign'

  def load(advertiser)
    super(advertiser_id: advertiser.id)
  end
end