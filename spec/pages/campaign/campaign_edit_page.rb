class CampaignEditPage < SitePrism::Page
  set_url '/campaigns/{campaign_id}/edit'
  set_url_matcher /campaigns\/\w{2,5}-[a-zA-Z0-9]{10}\/edit/

  section :form, CampaignFormSection, 'form.edit_campaign'

  def load(campaign)
    super(campaign_id: campaign.id)
  end
end