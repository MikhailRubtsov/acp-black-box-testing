class DashboardPage < SitePrism::Page
  set_url '/'
  set_url_matcher %r{\://([^/]+)/$}

  section :navbar, NavbarSection, 'nav.navbar'
  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
  section :table, DashboardListSection, 'table#dashboard.datatable'
end