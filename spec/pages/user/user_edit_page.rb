class UserEditPage < SitePrism::Page
  set_url '/users/{user_id}/edit'
  set_url_matcher /users\/\w{2,5}-[a-zA-Z0-9]{10}\/edit/

  section :form, UserFormSection, 'form.edit_user'

  def load(user)
    super(user_id: user.id)
  end
end