class UserShowPage < SitePrism::Page
  set_url '/users/{user_id}'
  set_url_matcher  /users\/\w{2,5}-[a-zA-Z0-9]{10}/

  section :navbar, NavbarSection, 'nav.navbar'
  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'

  def load(user)
    super(user_id: user.id)
  end
end