class UserNewPage < SitePrism::Page
  set_url '/users/new'
  set_url_matcher %r{/users/new}

  section :form, UserFormSection, 'form.new_user'
end