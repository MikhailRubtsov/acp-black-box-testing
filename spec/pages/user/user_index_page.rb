class UserIndexPage < SitePrism::Page
  set_url '/users'
  set_url_matcher %r{/users}

  section :modal, ModalWindowSection, 'div#confirmation-modal'
  section :users_list_table, UsersListSection, :xpath, '//table[@id="users"]'
  section :flash, FlashMessageSection, 'div.flash-container .col-md-12'
end