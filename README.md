# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Summary ###

* Black-box spec tests for BPD app
* ver. 0.1
* https://bitbucket.org/MikhailRubtsov/acp-black-box-testing

### Setting up ###

* Pull app from repository and run Dockerfile script to build app enviroment.
* Before run need to set RSPEC_ENV from ./support/config/test_config.yml
* Using RspecCore v3.5 and PhantomJs v2.0
* Tests runing by exec script bin/rspec

### Who do I talk to? ###

* Repo owner Rubtcov Mikhail
* email: abtop3750@gmail.com